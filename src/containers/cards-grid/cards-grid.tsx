import CardsGrid from '../../components/cards-grid/cards-grid';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { IActiveCard } from '../../store/cards/types';
import { GameCadrsUtil } from '../../store/game-cards.util';
import { changeActiveCards } from '../../store/cards/actions';
import * as React from 'react';

class CardsGridContainer extends React.Component<any, {}> {

    componentWillMount(): void {
        let gameCards = GameCadrsUtil.createGameCards(this.props.currentLevel);

        this.props.changeActiveCards(gameCards);
    }

    render(): React.ReactNode {
        return (
            <CardsGrid {...this.props} />
        )
    }
}

const mapStateToProps = (state: IAppState): any => ({
    cards: state.cards
});

const mapDispatchToProps = (dispatch: any) => ({
    changeActiveCards: (cards: IActiveCard[]) => dispatch(changeActiveCards(cards))
});

export default connect(mapStateToProps, mapDispatchToProps)(CardsGridContainer);