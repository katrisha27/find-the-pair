import $ = require('jquery');
import StartButton from '../../components/start-button/start-button';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { IActiveCard } from '../../store/cards/types';
import { GameCadrsUtil } from '../../store/game-cards.util';
import { startGame, finishGame } from '../../store/game/actions';
import { changeActiveCards, clearSuccessCards } from '../../store/cards/actions';
import * as React from 'react';

class GameControlContainer extends React.Component<any, {}> {

    startID: any;

    time: number = 30;
    initialTime: number = 30;

    constructor(props: any) {
        super(props);

        this.handleStartClick = this.handleStartClick.bind(this);
    }

    render(): React.ReactNode {
        return (
            <StartButton {...this.props} onStartBtnClick={this.handleStartClick}/>
        )
    }

    handleStartClick(): void {
        $('.fn-result').remove();

        this.startGame();
        this.endGameAfterTimeIsOver();
    }

    startGame(): void {
        this.disableControls();
        this.setTimer();
        this.props.startGame();
    }

    endGameAfterTimeIsOver(): void {
        setTimeout(() => {
            if (this.props.game) {
                this.endGame();
            }
        }, this.initialTime * 1000);
    }

    setTimer(): void {
        this.startID = setInterval(() => {
            let { successCards, activeCards } = this.props.cards;

            $('.fn-timer').text(`00:${this.formatTime(--this.time)}`);

            if (successCards.length === activeCards.length) {
                this.time = 0;

                this.endGame();
            }
        }, 1000);
    }

    endGame(): void {
        clearInterval(this.startID);
        this.props.finishGame();
        this.enableControls();
        this.showGameResult();

        setTimeout(() => this.refreshGame(), 1000);
    }

    showGameResult(): void {
        let { successCards, activeCards } = this.props.cards;

        if (successCards.length === activeCards.length) {
            $('.cards-grid').append(`<h3 class='fn-result'>CONGRATS! YOU WON!</h3>`);
        } else {
            $('.cards-grid').append(`<h3 class='fn-result'>SORRY! GAME OVER!</h3>`);
        }
    }

    refreshGame(): void {
        this.time = this.initialTime;

        let gameCards = GameCadrsUtil.createGameCards(this.props.level);

        this.props.changeActiveCards(gameCards);
        this.props.clearSuccessCards();

        $('.fn-timer').text(`00:${this.time}`);
        $('.fn-card').css('visibility', 'visible');
    }

    disableControls(): void {
        $('.fn-level, .fn-start').attr('disabled', 'disabled');
    }

    enableControls(): void {
        $('.fn-level, .fn-start').removeAttr('disabled');
    }

    formatTime(time: number): string {
        return time >= 0 && time < 10 ? `0${time}` : `${time}`;
    }
}

const mapStateToProps = (state: IAppState): any => ({
    level: state.level,
    game: state.game,
    cards: state.cards
});

const mapDispatchToProps = (dispatch: any) => ({
    startGame: () => dispatch(startGame()),
    finishGame: () => dispatch(finishGame()),
    changeActiveCards: (gameCards: IActiveCard[]) => dispatch(changeActiveCards(gameCards)),
    clearSuccessCards: () => dispatch(clearSuccessCards()),
});

export default connect(mapStateToProps, mapDispatchToProps)(GameControlContainer);