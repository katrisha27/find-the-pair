import $ = require('jquery');
import Card from '../../components/card/card';
import {
    selectFirstCard,
    selectSecondCard,
    clearSelectedCards,
    hideSelection,
    updateSuccessCards
} from '../../store/cards/actions';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { IActiveCard } from '../../store/cards/types';
import * as React from 'react';

class CardContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleCardClick = this.handleCardClick.bind(this);
    }

    render(): React.ReactNode {
        return (
            <Card {...this.props} onCardClick={this.handleCardClick} />
        )
    }

    handleCardClick(event: JQuery.Event): void {
        if (!this.props.game || !this.props.closed) {
            return;
        }

        let { firstSelectedCard, secondSelectedCard } = this.props.cards;
        let { value, closed, id } = this.props;
        let card = { value, closed, id };

        if (!(firstSelectedCard && firstSelectedCard.id)) {
            this.props.selectFirstCard(card);
        } else if (!(secondSelectedCard && secondSelectedCard.id)) {
            this.props.selectSecondCard(card);

            this.handleSelectionResult(card);
        }
    }

    handleSelectionResult(secondCard: IActiveCard): void {
        let { firstSelectedCard } = this.props.cards;
        let areCardsEqual = this.areCardsEqual(firstSelectedCard, secondCard);

        this.props.clearSelectedCards();

        areCardsEqual
            ? this.handleSuccessSelection(firstSelectedCard, secondCard)
            : this.handleFailedSelection();
    }

    areCardsEqual(firstCard: IActiveCard, secondCard: IActiveCard): boolean {
        return firstCard.value === secondCard.value;
    }

    handleSuccessSelection(firstCard: IActiveCard, secondCard: IActiveCard): void {
        this.props.updateSuccessCards([firstCard, secondCard]);

        setTimeout(() => $(`#${firstCard.id}, #${secondCard.id}`).css('visibility', 'hidden'), 200);
    }

    handleFailedSelection(): void {
        setTimeout(() => this.props.hideSelection(), 200);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    cards: state.cards,
    game: state.game
});

const mapDispatchToProps = (dispatch: any) => ({
    selectFirstCard: (card: IActiveCard) => dispatch(selectFirstCard(card)),
    selectSecondCard: (card: IActiveCard) => dispatch(selectSecondCard(card)),
    clearSelectedCards: () => dispatch(clearSelectedCards()),
    hideSelection: () => dispatch(hideSelection()),
    updateSuccessCards: (cards: IActiveCard[]) => dispatch(updateSuccessCards(cards))
});

export default connect(mapStateToProps, mapDispatchToProps)(CardContainer);