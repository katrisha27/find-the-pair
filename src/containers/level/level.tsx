import $ = require('jquery');
import LevelSelect from '../../components/level/level';
import { connect } from 'react-redux';
import { levels } from '../../appConfig';
import { withRouter } from 'react-router-dom';
import { ILevel } from '../../store/level/types';
import { IAppState } from '../../store/appTypes';
import { changeLevel } from '../../store/level/actions';
import { clearSuccessCards } from '../../store/cards/actions';
import * as React from 'react';

class LevelSelectContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleLevelChange = this.handleLevelChange.bind(this);
    }

    render(): React.ReactNode {
        return (
            <LevelSelect {...this.props} onLevelChange={this.handleLevelChange} />
        )
    }

    handleLevelChange(event: JQuery.Event): void {
        let activeLevel = (levels as any).find((level: ILevel) => level.id === $(event.target).val());

        this.props.changeLevel(activeLevel);
        this.props.clearSuccessCards();
        this.props.history.push(`/${activeLevel.name}`);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    level: state.level,
    cards: state.cards
});

const mapDispatchToProps = (dispatch: any) => ({
    changeLevel: (level: ILevel) => dispatch(changeLevel(level)),
    clearSuccessCards: () => dispatch(clearSuccessCards()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LevelSelectContainer));