import { ICard } from './store/cards/types';
import { ILevel } from './store/level/types';

export const levels: ILevel[] = [
    { id: 'lev-0', name: 'easy', weight: 2 },
    { id: 'lev-1', name: 'medium', weight: 3 },
    { id: 'lev-2', name: 'hard', weight: 4 }
];

export const cardsConfig = [
    'dog', 'cat', 'raccoon', 'bee',
    'mountains', 'map', 'trekking', 'lotus',
    'tree', 'coffee', 'candy', 'gingerbread',
    'banana', 'apple', 'bread', 'girl',
    'snowboarder', 'photocamera', 'sneakers', 'hat',
    'rick', 'moon', 'yoga', 'soccer',
    'lake', 'laptop', 'sunglasses', 'pumpkin',
    'morty', 'duck', 'c3po', 'hobbit'
];

export const cards: ICard[] = cardsConfig.reduce((config, card) => {
    config.push({
        value: card,
        closed: true
    });

    return config;
}, []);