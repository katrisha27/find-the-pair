import LevelContainer from '../../containers/level/level';
import GameControlContainer from '../../containers/game-control/game-control';
import * as React from 'react';

const Layout = ({ ...props }) => (
    <div className='layout'>
        <div className='controls'>
            <div className='controls__row'>
                <LevelContainer />
                <GameControlContainer />
            </div>
            <div className='controls__row'>
                <div className='fn-timer timer'>00:30</div>
            </div>
        </div>
        <div className='content'>
            {...props.content}
        </div>
    </div>
);

export default Layout;