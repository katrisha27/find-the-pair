import Layout from '../../components/layout/layout';
import CardsGridContainer from '../../containers/cards-grid/cards-grid';
import * as React from 'react';
import { levels } from '../../appConfig';

const Easy = ({ ...props }) => (
    <Layout content={
        <CardsGridContainer currentLevel={levels[0]} />
    } />
);

export default Easy;