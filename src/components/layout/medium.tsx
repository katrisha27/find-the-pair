import Layout from '../../components/layout/layout';
import CardsGridContainer from '../../containers/cards-grid/cards-grid';
import * as React from 'react';
import { levels } from '../../appConfig';

const Medium = ({ ...props }) => (
    <Layout content={
        <CardsGridContainer currentLevel={levels[1]} />
    } />
);

export default Medium;