import CardContainer from '../../containers/card/card';
import { IActiveCard } from '../../store/cards/types';
import * as React from 'react';

const CardsGrid = ({ ...props }) => (
    <div className='cards-grid' style={{ width: `${2 * props.currentLevel.weight * 4 + 2 * props.currentLevel.weight}em` }}>
        {props.cards.activeCards.map((card: IActiveCard, index: number) => (
                <CardContainer key={index} {...card} />
            ))
        }
    </div>
);

export default CardsGrid;
