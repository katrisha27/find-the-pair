import * as React from 'react';
import { levels } from '../../appConfig';
import { ILevel } from '../../store/level/types';

const LevelSelect = ({ ...props }) => (
    <select name='level' className='fn-level level' onChange={props.onLevelChange} defaultValue={props.level.id}>
        {levels.map((level: ILevel) => (
            <option key={level.id} value={level.id}>{level.name}</option>
        ))}
    </select>
);

export default LevelSelect;
