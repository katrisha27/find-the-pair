import * as React from 'react';

const StartButton = ({ ...props }) => (
    <button className="fn-start start" onClick={props.onStartBtnClick}>Start</button>
);

export default StartButton;
