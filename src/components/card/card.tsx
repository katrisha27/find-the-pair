import * as React from 'react';

const Card = ({ ...props }) => (
    <div
        id={props.id}
        className='fn-card card'
        onClick={props.onCardClick}
        style={{backgroundImage: 'url(' + require(`../../images/${props.closed ? 'closed' : props.value}.png`) + ')'}}>
    </div>
);

export default Card;
