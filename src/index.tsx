import rootReducer from './store/appTypes';
import Easy from './components/layout/easy';
import Hard from './components/layout/hard';
import Medium from './components/layout/medium';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { initialState } from './store/initialState';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as React from 'react';
import './styles/main.css';

const logger = createLogger();
export const store = createStore(rootReducer, initialState, applyMiddleware(logger));

render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route path='/*/*' component={Easy} />
                <Route path='/easy' exact={true} component={Easy} />
                <Route path='/medium' exact={true} component={Medium} />
                <Route path='/hard' exact={true} component={Hard} />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);
