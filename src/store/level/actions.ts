import { ILevel, IChangeLevelAction } from './types';
import { ActionCreator } from 'redux';

/**
 * CHANGE GAME LEVEL
 */
export const changeLevel: ActionCreator<IChangeLevelAction> = (level: ILevel) => ({
    type: '@@level/CHANGE_LEVEL',
    level
});
