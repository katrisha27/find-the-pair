import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { ILevel, LevelActions } from './types';

const LevelReducer: Reducer<ILevel> = (state: ILevel = initialState.level, action: LevelActions) => {
    switch (action.type) {
        case '@@level/CHANGE_LEVEL':
            return action.level;
        default:
            return state;
    }
};

export default LevelReducer;