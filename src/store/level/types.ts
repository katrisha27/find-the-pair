import { Action } from 'redux';

export interface ILevel {
    id: string;
    name: string;
    weight: number;
}

export interface IChangeLevelAction extends Action {
    type: '@@level/CHANGE_LEVEL';
    level: ILevel;
}

export type LevelActions = IChangeLevelAction;