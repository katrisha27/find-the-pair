import CardsReducer from './cards/reducer';
import LevelReducer from './level/reducer';
import GameReducer from './game/reducer';
import { ILevel } from './level/types';
import { ICardsState } from './cards/types';
import { combineReducers, Reducer} from 'redux';

export interface IAppState {
    cards: ICardsState;
    level: ILevel;
    game: boolean;
}

const rootReducer: Reducer<IAppState> = combineReducers<IAppState>({
    cards: CardsReducer,
    level: LevelReducer,
    game: GameReducer
} as any);

export default rootReducer;
