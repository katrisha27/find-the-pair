import {
    IActiveCard,
    ISelectFirstCardAction,
    ISelectSecondCardAction,
    IChangeActiveCardsAction,
    IClearSelectedCardsAction,
    IHideSelectionAction,
    IUpdateSuccessCardsAction,
    IClearSuccessCardsAction,
    IRefreshCardsAction
} from './types';
import { ActionCreator } from 'redux';

export const selectFirstCard: ActionCreator<ISelectFirstCardAction> = (card: IActiveCard) => ({
    type: '@@cards/FIRST_CARD_SELECTED',
    card
});

export const selectSecondCard: ActionCreator<ISelectSecondCardAction> = (card: IActiveCard) => ({
    type: '@@cards/SECOND_CARD_SELECTED',
    card
});

export const changeActiveCards: ActionCreator<IChangeActiveCardsAction> = (cards: IActiveCard[]) => ({
    type: '@@cards/CHANGE_ACTIVE_CARDS',
    cards
});

export const clearSelectedCards: ActionCreator<IClearSelectedCardsAction> = () => ({
    type: '@@cards/CLEAR_SELECTED_CARDS'
});

export const hideSelection: ActionCreator<IHideSelectionAction> = () => ({
    type: '@@cards/HIDE_SELECTION'
});

export const updateSuccessCards: ActionCreator<IUpdateSuccessCardsAction> = (cards: IActiveCard[]) => ({
    type: '@@cards/UPDATE_SUCCESS_CARDS',
    cards
});

export const clearSuccessCards: ActionCreator<IClearSuccessCardsAction> = () => ({
    type: '@@cards/CLEAR_SUCCESS_CARDS'
});

export const refreshCards: ActionCreator<IRefreshCardsAction> = () => ({
    type: '@@cards/REFRESH_CARDS_TO_INITIAL'
});
