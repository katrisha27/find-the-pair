import { Action } from 'redux';

export interface ICard {
    value?: string;
    closed?: boolean;
}

export interface IActiveCard extends ICard {
    id?: string;
}

export interface ICardsState {
    activeCards?: IActiveCard[],
    firstSelectedCard?: IActiveCard,
    secondSelectedCard?: IActiveCard,
    successCards?: IActiveCard[]
}

export interface ISelectFirstCardAction extends Action {
    type: '@@cards/FIRST_CARD_SELECTED';
    card: IActiveCard;
}

export interface ISelectSecondCardAction extends Action {
    type: '@@cards/SECOND_CARD_SELECTED';
    card: IActiveCard;
}

export interface IChangeActiveCardsAction extends Action {
    type: '@@cards/CHANGE_ACTIVE_CARDS';
    cards: IActiveCard[];
}

export interface IClearSelectedCardsAction extends Action {
    type: '@@cards/CLEAR_SELECTED_CARDS';
}

export interface IHideSelectionAction extends Action {
    type: '@@cards/HIDE_SELECTION';
}

export interface IUpdateSuccessCardsAction extends Action {
    type: '@@cards/UPDATE_SUCCESS_CARDS';
    cards: IActiveCard[];
}

export interface IClearSuccessCardsAction extends Action {
    type: '@@cards/CLEAR_SUCCESS_CARDS'
}

export interface IRefreshCardsAction extends Action {
    type: '@@cards/REFRESH_CARDS_TO_INITIAL'
}

export type CardsActions = ISelectFirstCardAction
    | ISelectSecondCardAction
    | IChangeActiveCardsAction
    | IClearSelectedCardsAction
    | IHideSelectionAction
    | IUpdateSuccessCardsAction
    | IClearSuccessCardsAction
    | IRefreshCardsAction;