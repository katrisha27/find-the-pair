import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { ICardsState, CardsActions, IActiveCard } from './types';

const CardsReducer: Reducer<ICardsState> = (state: ICardsState = initialState.cards, action: CardsActions) => {
    switch (action.type) {
        case '@@cards/CHANGE_ACTIVE_CARDS':
            return {
                ...state,
                activeCards: action.cards
            };
        case '@@cards/FIRST_CARD_SELECTED':
            let firstCardIndex = [].slice.call(state.activeCards).findIndex((card: IActiveCard) => card.id === action.card.id);
            let elemsBeforeFirst = state.activeCards.slice(0, firstCardIndex);
            let elemsAfterFirst = state.activeCards.slice(firstCardIndex + 1);

            return {
                ...state,
                activeCards: [
                    ...elemsBeforeFirst,
                    { ...action.card, closed: false },
                    ...elemsAfterFirst
                ],
                firstSelectedCard: {
                    ...action.card,
                    closed: false
                }
            };
        case '@@cards/SECOND_CARD_SELECTED':
            let secondCardIndex = [].slice.call(state.activeCards).findIndex((card: IActiveCard) => card.id === action.card.id);
            let elemsBeforeSecond = state.activeCards.slice(0, secondCardIndex);
            let elemsAfterSecond = state.activeCards.slice(secondCardIndex + 1);

            return {
                ...state,
                activeCards: [
                    ...elemsBeforeSecond,
                    { ...action.card, closed: false },
                    ...elemsAfterSecond
                ],
                secondSelectedCard: {
                    ...action.card,
                    closed: false
                }
            };
        case '@@cards/CLEAR_SELECTED_CARDS':
            return {
                ...state,
                firstSelectedCard: {},
                secondSelectedCard: {}
            };
        case '@@cards/HIDE_SELECTION':
            return {
                ...state,
                activeCards: [...state.activeCards.map((card: IActiveCard) => {
                    card.closed = true;

                    return card;
                })]
            };
        case '@@cards/UPDATE_SUCCESS_CARDS':
            return {
                ...state,
                successCards: [...state.successCards, ...action.cards]
            };
        case '@@cards/CLEAR_SUCCESS_CARDS':
            return {
                ...state,
                successCards: []
            };
        case '@@cards/REFRESH_CARDS_TO_INITIAL':
            return initialState.cards;
        default:
            return state;
    }
};

export default CardsReducer;