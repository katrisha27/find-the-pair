import { cards } from '../appConfig';
import { ILevel } from './level/types';
import { ICard, IActiveCard } from './cards/types';

export abstract class GameCadrsUtil {
    static createGameCards(level: ILevel): IActiveCard[] {
        let uniqueCardsNumber = 2 * level.weight ** 2;
        let uniqueLevelCards = cards.slice(0, uniqueCardsNumber);
        let levelCards = [...uniqueLevelCards, ...uniqueLevelCards];
        let gameCards = levelCards.reduce((config: any, card: ICard, index: number) => {
            config.push({
                value: card.value,
                closed: card.closed,
                id: `card-${index}`
            });

            return config;
        }, []);

        return this.shuffleCards(gameCards);
    }

    static shuffleCards(cards: IActiveCard[]): IActiveCard[] {
        return cards.sort(() => Math.random() - 0.5);
    }
}