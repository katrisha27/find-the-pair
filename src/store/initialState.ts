import { levels } from '../appConfig';
import { IAppState } from './appTypes';

export const initialState: IAppState = {
    cards: {
        activeCards: [],
        successCards: [],
        firstSelectedCard: {},
        secondSelectedCard: {}
    },
    level: levels[0],
    game: false
};