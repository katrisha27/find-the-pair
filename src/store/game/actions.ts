import {
    IStartGameAction,
    IFinishGameAction
} from './types';
import { ActionCreator } from 'redux';

/**
 *  GAME STARTED
 */
export const startGame: ActionCreator<IStartGameAction> = () => ({
    type: '@@game/GAME_STARTED'
});

/**
 * GAME FINISHED
 */
export const finishGame: ActionCreator<IFinishGameAction> = () => ({
    type: '@@game/GAME_FINISHED'
});