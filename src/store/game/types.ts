import { Action } from 'redux';

export interface IStartGameAction extends Action {
    type: '@@game/GAME_STARTED';
}

export interface IFinishGameAction extends Action {
    type: '@@game/GAME_FINISHED';
}
export type GameActions = IStartGameAction
    | IFinishGameAction;