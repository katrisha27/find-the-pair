import { Reducer } from 'redux';
import { GameActions } from './types';
import { initialState } from '../initialState';

const GameReducer: Reducer<boolean> = (state: boolean = initialState.game, action: GameActions) => {
    switch (action.type) {
        case '@@game/GAME_STARTED':
            return true;
        case '@@game/GAME_FINISHED':
            return false;
        default:
            return state;
    }
};

export default GameReducer;